import random
import os, os.path
import shutil
import zipfile

path_to_zip='/scratch/mondego/local/farima/toolsPairs/bytype_samples/anonymized-zip-files/4.zip'
path_to_types='/scratch/mondego/local/farima/toolsPairs/bytype_samples/nicad/'

type1set=[]
type2set=[]
type3set=[]

with open(path_to_types+'nicad-type1-samples.txt') as f:
    for line in f:
        line=line.replace('\n','')
        type1set.append(line)

with open(path_to_types + 'nicad-type2-samples.txt') as f:
    for line in f:
        line=line.replace('\n','')
        type2set.append(line)

with open(path_to_types+'nicad-type3-samples.txt') as f:
    for line in f:
        line=line.replace('\n','')
        type3set.append(line)

# archive = zipfile.ZipFile(path_to_zip, 'r')
# data = archive.read('nicad-alltypes-samples-shuf.txt')

num1=0
num2=0
num3=0

with zipfile.ZipFile(path_to_zip) as z:
    for filename in z.namelist():
        if not os.path.isdir(filename):
            # read the file
            with z.open(filename) as f:
                for line in f:
                    line = line.replace('\n', '')
                    if (line in type1set):
                        num1 += 1
                    if (line in type2set):
                        num2 += 1
                    if (line in type3set):
                        num3 += 1


print('type1: '+str(num1))
print('type2: '+str(num2))
print('type3: '+str(num3))


