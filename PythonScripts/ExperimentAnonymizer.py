import random
import os, os.path
import shutil

path_to_files='D:\\clones-type2\\'
new_path='D:\\testanon\\'
file_mapping=open('D:\\mappings.txt','w')

num_files=(len([name for name in os.listdir(path_to_files) if os.path.isfile(os.path.join(path_to_files, name))]))
file_num_mapping={}
rands_set=[]
idx=1
rand=None





for f in os.listdir(path_to_files):
    if(os.path.isfile(os.path.join(path_to_files, f))):
        #print(f+" : "+str(os.path.getsize(os.path.join(path_to_files, f))/1000))
        while rand in rands_set or rand is None:
            rand = random.randint(1, num_files)
        file_num_mapping[f]=rand
        rands_set.append(rand)


for f in os.listdir(path_to_files):
    file_fullpath=os.path.join(path_to_files, f)
    if(os.path.isfile(file_fullpath)):
        shutil.copy(file_fullpath, os.path.join(new_path, str(file_num_mapping.get(f))+'.zip'))
        file_mapping.write(str(file_num_mapping.get(f))+" : "+str(f)+'\n')




