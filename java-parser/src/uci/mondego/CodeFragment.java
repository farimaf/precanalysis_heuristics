package uci.mondego;

/**
 * Created by Farima on 9/14/2018.
 */
public class CodeFragment {
    public String mainPath;
    public String dirName;
    public String fileName;
    public int startline;
    public int endline;

    public String getMainPath() {
        return mainPath;
    }

    public void setMainPath(String mainPath) {
        this.mainPath = mainPath;
    }



    public String getDirName() {
        return dirName;
    }

    public void setDirName(String dirName) {
        this.dirName = dirName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getStartline() {
        return startline;
    }

    public void setStartline(int startline) {
        this.startline = startline;
    }

    public int getEndline() {
        return endline;
    }

    public void setEndline(int endline) {
        this.endline = endline;
    }

    @Override
    public String toString() {
        return dirName+","+fileName+","+startline+","+endline;
    }
}
