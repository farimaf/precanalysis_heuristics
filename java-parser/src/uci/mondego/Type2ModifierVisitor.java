package uci.mondego;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Queue;

import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.nodeTypes.NodeWithIdentifier;
import com.github.javaparser.ast.type.ReferenceType;
import com.github.javaparser.ast.visitor.TreeVisitor;

public class Type2ModifierVisitor extends TreeVisitor {
    File file;
    String fileContent;
    Path path;
    String methodBodyTransformed="";
    public String getMethodBodyTransformed() {
        return methodBodyTransformed;
    }



    public Type2ModifierVisitor(File file) {
        super();
        this.file = file;
        this.path = file.toPath();
    }

    public Type2ModifierVisitor(String content, Path path) {
        super();
        this.fileContent = content;
        this.path = path;
    }

    @Override
    public void process(Node node) {
        if (node instanceof ClassOrInterfaceDeclaration) {
            //System.out.println(node);
            applyType2Normalizations(node);
            //System.out.println(node);
            methodBodyTransformed=node.toString();
        }
    }

    private static void applyType2Normalizations(Node nodeMain){
        Queue<Node> queue=new LinkedList<>();
        queue.add(nodeMain);
        while (!queue.isEmpty()){
            Node node=queue.poll();
            if (node instanceof NodeWithIdentifier){
                ((NodeWithIdentifier) node).setIdentifier("X");
            }
            else if (node instanceof StringLiteralExpr){
                ((StringLiteralExpr) node).setString("STR");
            }
            else if(node instanceof CharLiteralExpr){
                ((CharLiteralExpr) node).setChar('C');
            }
            else if(node instanceof IntegerLiteralExpr){
                ((IntegerLiteralExpr) node).setInt(1);
            }
            else if(node instanceof LongLiteralExpr){
                ((LongLiteralExpr) node).setLong(10);
            }
            else if(node instanceof DoubleLiteralExpr){
                ((DoubleLiteralExpr) node).setDouble(0.1);
            }
            else if(node instanceof BooleanLiteralExpr){
                ((BooleanLiteralExpr) node).setValue(true);
            }
            else {
                for (Node n:node.getChildNodes()){
                    queue.add(n);
                }
            }
        }

    }

    private static void applyType2NormalizationsRec(Node node){
        System.out.println(node.getClass());
        System.out.println(node);
        if (node instanceof NodeWithIdentifier){
           ((NodeWithIdentifier) node).setIdentifier("X");
        }
        else if (node instanceof StringLiteralExpr){
            ((StringLiteralExpr) node).setString("STR");
        }
        else if(node instanceof CharLiteralExpr){
            ((CharLiteralExpr) node).setChar('C');
        }
        else if(node instanceof IntegerLiteralExpr){
            ((IntegerLiteralExpr) node).setInt(1);
        }
        else if(node instanceof LongLiteralExpr){
            ((LongLiteralExpr) node).setLong(10);
        }
        else if(node instanceof DoubleLiteralExpr){
            ((DoubleLiteralExpr) node).setDouble(0.1);
        }
        else if(node instanceof BooleanLiteralExpr){
            ((BooleanLiteralExpr) node).setValue(true);
        }
        else {
            for (Node n:node.getChildNodes()){
                applyType2Normalizations(n);
            }
        }
    }


}
