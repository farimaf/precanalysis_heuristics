package uci.mondego;

import java.sql.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Farima on 9/15/2018.
 */
public class DatabaseQuery {
    Connection connection = null;
    private Map<CodeFragment, Integer> cache = lruCache(500000*2*12);

    DatabaseQuery(){
        getConnection();
    }

    private void getConnection() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://amazon.ics.uci.edu:3306/bcb?" +
                    "", "sourcerer", "donotpublish");
            System.out.println("Connected to MySql DB");

        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public int getNumToken(CodeFragment codeFragment){
        if (cache.containsKey(codeFragment)){
            return cache.get(codeFragment);
        }
        else {
            int tokenNum = 0;
            try {
                PreparedStatement pst = null;
                ResultSet rs = null;
                pst = connection.prepareStatement("SELECT * FROM bcb.FUNCTIONS WHERE NAME=? AND TYPE=? AND STARTLINE=? AND ENDLINE=?");
                pst.setString(1, codeFragment.fileName);
                pst.setString(2, codeFragment.dirName);
                pst.setInt(3, codeFragment.startline);
                pst.setInt(4, codeFragment.endline);
                rs = pst.executeQuery();
                if (rs.next()) {
                    tokenNum = rs.getInt("TOKENS");
                    cache.put(codeFragment,tokenNum);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return tokenNum;
        }
    }

    public static <K, V> Map<K, V> lruCache(final int maxSize) {
        return Collections.synchronizedMap(
                new LinkedHashMap<K, V>(maxSize * 4 / 3, 0.75f, true) {
                    @Override
                    protected boolean removeEldestEntry(
                            Map.Entry<K, V> eldest) {
                        return size() > maxSize;
                    }
                });
    }
}
