package uci.mondego;

import java.io.PrintWriter;

/**
 * Created by Farima on 9/18/2018.
 */
public class ExceptionLogger {
    static PrintWriter printWriterExceptionsLog=null;
    public static void log(Exception ex){
        try {
            if (printWriterExceptionsLog == null) {
                printWriterExceptionsLog = new PrintWriter("log_exceptions.txt");
            }
            printWriterExceptionsLog.append(ex.toString()+System.lineSeparator());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void closeLogging(){
        if (printWriterExceptionsLog!=null) printWriterExceptionsLog.close();
    }
}
