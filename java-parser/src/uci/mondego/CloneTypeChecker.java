package uci.mondego;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Farima on 9/14/2018.
 */
public class CloneTypeChecker {
    private static Map<String, String> cacheType1 = lruCache(500000*2*24);
    private static Map<String, String> cacheType2 = lruCache(500000*2*24);
    public static void main(String[] args) {
        CloneTypeChecker cloneTypeChecker=new CloneTypeChecker();
        JavaTransformationParser javaTransformationParser=new JavaTransformationParser();
        TypeBasedWriter typeBasedWriter=null;
        //DatabaseQuery databaseQuery=new DatabaseQuery();
//        String path="D:\\PhD\\Clone\\Precision-Analysis\\cps.txt";
//        String dir="input_test";
//        String file="Node.java";
//        int startline=14;
//        int endline=29;
//        String clonepiarsFile="D:\\PhD\\Clone\\Precision-Analysis\\cps.txt";
//        String mainPath="D:\\PhD\\Clone\\Precision-Analysis";
//        String toolName="scc";
//        String mainPath="D:\\PhD\\Clone\\bcb_dataset_java_source";
//        String clonepiarsFile="D:\\PhD\\Clone\\demo_clonepairs\\scc_problem.txt";
//        String toolName="scc";
        if (args.length>0 ){//remove true and uncomment the next two lines
            String mainPath=args[0];
            String clonepiarsFile=args[1];
            String toolName=args[2];
            typeBasedWriter=new TypeBasedWriter(toolName);
            BufferedReader bufferedReader=null;
            int lineNum=0;
            try {
                bufferedReader=new BufferedReader(new FileReader(clonepiarsFile));
                String line="";
                while ((line=bufferedReader.readLine())!=null) {
                    lineNum++;
                    System.out.println("processing line num: "+lineNum);
                    line = line.replace("\n", "");
                    String hashType1Code1="",hashType1Code2="";
                    String hashType2Code1="",hashType2Code2="";
                    String sourceCode1="",sourceCode2="";
                    boolean isType1,isType2;
                    CodeFragment[] fetchedPair = cloneTypeChecker.fetchCodeFragmentPath(mainPath, line);
                    //if (databaseQuery.getNumToken(fetchedPair[0]) >= 50 && databaseQuery.getNumToken(fetchedPair[1]) >= 50) {
                        if (cacheType1.containsKey(fetchedPair[0].toString())){
                            hashType1Code1=cacheType1.get(fetchedPair[0].toString());
                        }
                        if (cacheType1.containsKey(fetchedPair[1].toString())){
                            hashType1Code2=cacheType1.get(fetchedPair[1].toString());
                        }
                        if(hashType1Code1.length()==0){
                            sourceCode1 = new MethodBodyLocator().locate(fetchedPair[0]);
                            String source1_flat = cloneTypeChecker.getFlatString(sourceCode1);
                            hashType1Code1 = cloneTypeChecker.getHash(source1_flat);

                        }
                        if(hashType1Code2.length()==0){
                            sourceCode2 = new MethodBodyLocator().locate(fetchedPair[1]);
                            String source2_flat=cloneTypeChecker.getFlatString(sourceCode2);
                            hashType1Code2=cloneTypeChecker.getHash(source2_flat);
                        }

                        isType1 = hashType1Code1.equals(hashType1Code2);

                        if (isType1){
                            typeBasedWriter.writePairByType(line,"1");
                        }
                        else if (!isType1) {
                            if (cacheType2.containsKey(fetchedPair[0].toString())){
                                hashType2Code1=cacheType2.get(fetchedPair[0].toString());
                            }
                            if (cacheType2.containsKey(fetchedPair[1].toString())){
                                hashType2Code2=cacheType2.get(fetchedPair[1].toString());
                            }
                            if(hashType2Code1.length()==0){
                                if (sourceCode1.length()==0) sourceCode1 = new MethodBodyLocator().locate(fetchedPair[0]);
                                String source1Transformed = javaTransformationParser.applyTransformation(fetchedPair[0], sourceCode1);
                                if (source1Transformed==null){
                                    typeBasedWriter.writeError(line);
                                    continue;
                                }
                                String source1Flat = cloneTypeChecker.getFlatString(source1Transformed);
                                hashType2Code1 = cloneTypeChecker.getHash(source1Flat);

                            }
                            if(hashType2Code2.length()==0){
                                if (sourceCode2.length()==0) sourceCode2 = new MethodBodyLocator().locate(fetchedPair[1]);
                                String source2Transformed = javaTransformationParser.applyTransformation(fetchedPair[1], sourceCode2);
                                if (source2Transformed==null){
                                    typeBasedWriter.writeError(line);
                                    continue;
                                }
                                String source2Flat = cloneTypeChecker.getFlatString(source2Transformed);
                                hashType2Code2 = cloneTypeChecker.getHash(source2Flat);
                            }
                            isType2=hashType2Code1.equals(hashType2Code2);
                            if (isType2) {
                                typeBasedWriter.writePairByType(line,"2");
                            }
                            else if(!isType2){
                                typeBasedWriter.writePairByType(line,"3");
                            }
                        }
                    //}
                }
            }
            catch (IOException e){
                e.printStackTrace();
                System.out.println("wrong file name "+ args[1]);
            }
            typeBasedWriter.finishWriting();
            ExceptionLogger.closeLogging();
        }
        else {
            System.out.println("Please provide parameters");
        }
    }


    private CodeFragment[] fetchCodeFragmentPath(String mainPath, String pairLine){
        CodeFragment[] codeFragment=new CodeFragment[2];
        codeFragment[0]=new CodeFragment();
        codeFragment[1]=new CodeFragment();
        String[] lineSplitted=pairLine.split(",");
        codeFragment[0].mainPath=mainPath;
        codeFragment[1].mainPath=mainPath;
        codeFragment[0].dirName=lineSplitted[0];
        codeFragment[0].fileName=lineSplitted[1];
        codeFragment[0].startline=Integer.parseInt(lineSplitted[2]);
        codeFragment[0].endline=Integer.parseInt(lineSplitted[3]);
        codeFragment[1].dirName=lineSplitted[4];
        codeFragment[1].fileName=lineSplitted[5];
        codeFragment[1].startline=Integer.parseInt(lineSplitted[6]);
        codeFragment[1].endline=Integer.parseInt(lineSplitted[7]);
        return codeFragment;

    }

    private String getFlatString(String codeCody){
        String flatStr = "";
//        String pattern_inline_comments = "//.*(\\n|\\r|\\r\\n)";
//        String pattern_block_comments = "/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/";
//        pattern_block_comments="/\\*.*\\*/";
        String pattern_new_lines = "\\n|\\r|\\r\\n";
        String pattern_whitespace = "\\s+";
//        flatStr = codeCody.replaceAll(pattern_inline_comments, "");
//        flatStr = flatStr.replaceAll(pattern_block_comments, "");
        flatStr=removeComments(codeCody);
        flatStr = flatStr.replaceAll(pattern_new_lines, "");
        flatStr = flatStr.replaceAll(pattern_whitespace, "");
        return flatStr;
    }

    private  String removeComments(String input) {
        String regexLineComment = "//.*(\\n|\\r|\\r\\n)";
        String x = input.replaceAll(regexLineComment, " ");
        x = x.replaceAll("\\n|\\r|\\r\\n", " ");
        String regexPattern = "/\\*.*\\*/";
        // String regexEnd = "*/";
        x = x.replaceAll(regexPattern, "");
        return x;
    }

    private String getHash(String flatString){
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(flatString.getBytes(StandardCharsets.UTF_8));
            String encoded = Base64.getEncoder().encodeToString(hash);
            return encoded;
        }
        catch (Exception e){
            ExceptionLogger.log(e);
        }
        return "";
    }

    private boolean isType1(String source1,String source2){
        String source1_flat=getFlatString(source1);
        String source2_flat=getFlatString(source2);
        return getHash(source1_flat).equals(getHash(source2_flat));
    }

    private boolean isType2(CodeFragment codeFragment1, String source1,CodeFragment codeFragment2,String source2){
        JavaTransformationParser javaTransformationParser=new JavaTransformationParser();
        javaTransformationParser.applyTransformation(codeFragment1,source1);
        String source1Transformed=javaTransformationParser.applyTransformation(codeFragment1,source1);
        String source2Transformed=javaTransformationParser.applyTransformation(codeFragment2,source2);
        String source1Flat=getFlatString(source1Transformed);
        String source2Flat=getFlatString(source2Transformed);
//        System.out.println(source1Flat);
//        System.out.println(source2Flat);
        return getHash(source1Flat).equals(getHash(source2Flat));
    }

    private   static <K, V> Map<K, V> lruCache(final int maxSize) {
        return Collections.synchronizedMap(
                new LinkedHashMap<K, V>(maxSize * 4 / 3, 0.75f, true) {
                    @Override
                    protected boolean removeEldestEntry(
                            Map.Entry<K, V> eldest) {
                        return size() > maxSize;
                    }
                });
    }


}
