package uci.mondego;

import java.io.PrintWriter;

/**
 * Created by Farima on 9/15/2018.
 */
public class TypeBasedWriter {
    PrintWriter printWriterType1=null;
    PrintWriter printWriterType2=null;
    PrintWriter printWriterType3=null;
    PrintWriter printWriterError=null;
    String toolName="";
    public TypeBasedWriter(String toolName){
        this.toolName=toolName;
    }

    public  void writePairByType(String clonePairLine, String type){

        try {
            switch (type){
                case "1":
                    if (printWriterType1==null){
                        printWriterType1=new PrintWriter(toolName+"_clonePairsType"+type+".txt");
                    }
                    printWriterType1.append(clonePairLine+System.lineSeparator());
                    break;
                case "2":
                    if (printWriterType2==null){
                    printWriterType2=new PrintWriter(toolName+"_clonePairsType"+type+".txt");
                    }
                    printWriterType2.append(clonePairLine+System.lineSeparator());
                    break;
                case "3":
                    if (printWriterType3==null){
                    printWriterType3=new PrintWriter(toolName+"_clonePairsType"+type+".txt");
                    }
                    printWriterType3.append(clonePairLine+System.lineSeparator());
                    break;
            }
        }
        catch (Exception e){
            ExceptionLogger.log(e);
        }
    }

    public  void finishWriting(){
        if (printWriterType1!=null) printWriterType1.close();
        if (printWriterType2!=null) printWriterType2.close();
        if (printWriterType3!=null) printWriterType3.close();
        if (printWriterError!=null) printWriterError.close();
    }
    public void writeError(String erroredLine){
        try {
            if (printWriterError == null) {
                printWriterError = new PrintWriter("erroredFiles.txt");
            }
            printWriterError.append(erroredLine+System.lineSeparator());
        }
        catch (Exception e){
            ExceptionLogger.log(e);
        }
    }

}
