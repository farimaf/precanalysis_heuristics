package uci.mondego;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Farima on 9/14/2018.
 */
public class MethodBodyLocator {
    public String locate( CodeFragment codeFragment){
        StringBuffer codeBody=new StringBuffer("");
        String filePath=codeFragment.mainPath+ File.separator+codeFragment.dirName+File.separator+codeFragment.fileName;
        try {
            BufferedReader bufferedReader=new BufferedReader(new FileReader(filePath));
            int i=1;
            String line="";
            while ((line=bufferedReader.readLine())!=null){
                if(i>=codeFragment.startline && i<=codeFragment.endline){
                    codeBody.append(line+System.lineSeparator());
                }
                if (i>codeFragment.endline) break;
                i++;
            }
            bufferedReader.close();
        }
        catch (IOException e){
            ExceptionLogger.log(e);
        }

        return codeBody.toString();
    }
}
