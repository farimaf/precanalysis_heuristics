package uci.mondego;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.visitor.TreeVisitor;
import com.github.javaparser.utils.SourceRoot;

import java.io.*;

public class JavaTransformationParser {
    public String outputDirPath;
    public String metricsFileName;
    public String tokensFileName;
    public String bookkeepingFileName;
    public String errorFileName;
    public static String prefix;
    public static String fileIdPrefix;
    public static long FILE_COUNTER;
    public static long METHOD_COUNTER;
    private IInputProcessor inputProcessor;

    public JavaTransformationParser(String inputFilePath) {
        this.metricsFileName = "mlcc_input.file";
        this.bookkeepingFileName = "bookkeeping.file";
        this.errorFileName = "error_metrics.file";
        this.tokensFileName = "scc_input.file";
        JavaTransformationParser.prefix = this.getBaseName(inputFilePath);
        JavaTransformationParser.fileIdPrefix = JavaTransformationParser.prefix;// "1";
        this.outputDirPath = JavaTransformationParser.prefix + "_metric_output";
        File outDir = new File(this.outputDirPath);
        if (!outDir.exists()) {
            outDir.mkdirs();
        }
    }
    public JavaTransformationParser(){

    }

    private String getBaseName(String path) {
        File inputFile = new File(path);
        String fileName = inputFile.getName();
        int pos = fileName.lastIndexOf(".");
        if (pos > 0) {
            fileName = fileName.substring(0, pos);
        }
        return fileName;
    }

    private void initializeWriters() throws IOException {
        FileWriters.metricsFileWriter = Util.openFile(this.outputDirPath + File.separator + this.metricsFileName,
                false);

        FileWriters.errorPw = Util.openFile(this.outputDirPath + File.separator + this.errorFileName, false);
        FileWriters.bookkeepingWriter = Util.openFile(this.outputDirPath + File.separator + this.bookkeepingFileName,
                false);
        FileWriters.tokensFileWriter = Util.openFile(this.outputDirPath + File.separator + this.tokensFileName, false);
    }

    private void closeWriters() {
        Util.closeOutputFile(FileWriters.metricsFileWriter);
        Util.closeOutputFile(FileWriters.errorPw);
        Util.closeOutputFile(FileWriters.tokensFileWriter);
        Util.closeOutputFile(FileWriters.bookkeepingWriter);
    }

    private void handleInput(String inputMode, String filename) {
        BufferedReader br;
        try {
            this.initializeWriters();
            if ("dir".equals(inputMode)) {
                this.inputProcessor = new FolderInputProcessor();
            } else if ("tgz".equals(inputMode)) {
                this.inputProcessor = new TGZInputProcessor();
            } else if ("zip".equals(inputMode)) {
                this.inputProcessor = new ZipInputProcessor();
            }

            br = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"));
            String line = null;
            while ((line = br.readLine()) != null && line.trim().length() > 0) {
                line = line.trim();
                this.inputProcessor.processInput(line);
            }
            try {
                br.close();
            } catch (IOException e) {
                System.out.println("WARN, couldn't close inputfile: " + filename);
            }

            this.closeWriters();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String applyTransformation(CodeFragment codeFragment, String sourceCode){
        try {
            File tempFile = writeTempFetchedFragment(codeFragment, sourceCode);
            //System.out.println("Transforming " + tempFile.getName());
            JavaTransformationParser.FILE_COUNTER++;
            CompilationUnit cu = JavaParser.parse(tempFile);
            TreeVisitor visitor = new Type2ModifierVisitor(tempFile);
            visitor.visitPreOrder(cu);
            String transformed= ((Type2ModifierVisitor)visitor).getMethodBodyTransformed();
            deleteTempFragmentFile(tempFile);
            return transformed;
        }
        catch (Exception e){
            ExceptionLogger.log(e);
        }
        return null;
    }
    public static void main(String[] args) throws FileNotFoundException {

//        System.out.println(sourceCode);
//        String filename="D:\\PhD\\Clone\\Precision-Analysis\\1.txt";
//        String inputMode = "dir";

//        javaTransformationParser.handleInput(inputMode, filename);
//        System.out.println("done!");
    }

    private File writeTempFetchedFragment(CodeFragment codeFragment,String body){
        File file=null;
        PrintWriter pw =null;
        StringBuffer wrappedBody=new StringBuffer("");
        wrappedBody.append("public class Wrapper{"+System.lineSeparator());
        wrappedBody.append(body);
        wrappedBody.append("}");
        try {
            pw= new PrintWriter(codeFragment.dirName + "_" + codeFragment.fileName + "_" +
                    codeFragment.startline + "_" + codeFragment.endline + ".java");
            pw.write(wrappedBody.toString());
            file=new File(codeFragment.dirName + "_" + codeFragment.fileName + "_" +
                    codeFragment.startline + "_" + codeFragment.endline + ".java");
        }
        catch (IOException e){
            ExceptionLogger.log(e);
        }
        finally {
            pw.close();
        }
        return file;
    }

    private void deleteTempFragmentFile(File file){
        file.delete();
//        if(file.delete())
//        {
//            System.out.println(file.getName()+" deleted");
//        }
//        else
//        {
//            System.out.println(file.getName()+" failed to delete");
//        }
    }


}