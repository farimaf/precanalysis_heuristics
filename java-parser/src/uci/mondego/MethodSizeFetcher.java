package uci.mondego;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseProblemException;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.TreeVisitor;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 * Created by Farima on 10/9/2018.
 */
public class MethodSizeFetcher {

    Connection dbconnection = null;
    private static Map<String, Integer> cache = lruCache(500000*2*24);


    MethodSizeFetcher(){
        //getDBConnection();
    }

    PrintWriter pwNOS=null;
    public static void main(String[] args) {
        //    String clonepiarsFile = "D:\\not_test.txt";
//    String mainPath = "D:\\PhD\\Clone\\bcb_dataset_java_source";
//String toolName="ccaligner";
        if (args.length>0) {
            String clonepiarsFile = args[0];
            String mainPath = args[1];
            String toolName=args[2];
            MethodSizeFetcher methodSizeFetcher = new MethodSizeFetcher();
            BufferedReader bufferedReader = null;
            int lineNum = 0;
            try {
                bufferedReader = new BufferedReader(new FileReader(clonepiarsFile));
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    lineNum++;
                    System.out.println("processing line num: " + lineNum);
                    line = line.replace("\n", "");
                    int NOS1 = -1;
                    int NOS2 = -1;
                    CodeFragment[] fetchedPair = methodSizeFetcher.fetchCodeFragmentPath(mainPath, line);
                    try {
                        if (cache.containsKey(fetchedPair[0].toString())) {
                            NOS1 = cache.get(fetchedPair[0].toString());
                        } else {
                            String sourceCode1 = new MethodBodyLocator().locate(fetchedPair[0]);
                            File file1 = methodSizeFetcher.writeTempFetchedFragment(fetchedPair[0], sourceCode1);
                            CompilationUnit cu = JavaParser.parse(file1);
                            TreeVisitor visitor = new CustomVisitor(file1);
                            visitor.visitPreOrder(cu);
                            NOS1 = ((CustomVisitor) visitor).NOSforFarima;
                            methodSizeFetcher.deleteTempFragmentFile(file1);
                        }

                        if (cache.containsKey(fetchedPair[1].toString())) {
                            NOS2 = cache.get(fetchedPair[1].toString());
                        } else {
                            String sourceCode2 = new MethodBodyLocator().locate(fetchedPair[1]);
                            File file2 = methodSizeFetcher.writeTempFetchedFragment(fetchedPair[1], sourceCode2);
                            CompilationUnit cu = JavaParser.parse(file2);
                            TreeVisitor visitor = new CustomVisitor(file2);
                            visitor.visitPreOrder(cu);
                            NOS2 = ((CustomVisitor) visitor).NOSforFarima;
                            methodSizeFetcher.deleteTempFragmentFile(file2);
                        }
                        int avgNOS = (NOS1 + NOS2) / 2;
                        methodSizeFetcher.writeNOS(toolName, fetchedPair[0].toString() + "," + fetchedPair[1].toString() + "~" + avgNOS);
                    }
                    catch (ParseProblemException e){
                        continue;
                    }
                }

                methodSizeFetcher.pwNOS.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else {
            System.out.println("provide arguments");
        }
    }

    private void writeNOS(String toolName,String lineToWrite){
        try{
            if (pwNOS==null) pwNOS=new PrintWriter(toolName+"_NOS.txt");
            pwNOS.append(toolName+"~"+lineToWrite+System.lineSeparator());
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    private void deleteTempFragmentFile(File file){
        file.delete();

    }

    private int getLenFromDB(CodeFragment cf){
        int num=-1;
        try {
            PreparedStatement pst = null;
            ResultSet rs = null;
            pst = dbconnection.prepareStatement("SELECT TOKENS FROM bcb.FUNCTIONS\n" +
                    "WHERE NAME=? AND TYPE=? AND STARTLINE=? AND ENDLINE=?");
            pst.setString(1, cf.getFileName());
            pst.setString(2, cf.getDirName());
            pst.setInt(3, cf.getStartline());
            pst.setInt(4, cf.getEndline());
            rs = pst.executeQuery();
            if (rs.next()) {
                num = rs.getInt("TOKENS");
                //cache.put(codeFragment,tokenNum);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num;
    }

    private void getDBConnection() {


        try {
            Class.forName("com.mysql.jdbc.Driver");
            dbconnection = DriverManager.getConnection("jdbc:mysql://amazon.ics.uci.edu:3306/bcb?" +
                    "", "sourcerer", "donotpublish");
            System.out.println("Connected to MySql DB");

        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private CodeFragment[] fetchCodeFragmentPath(String mainPath, String pairLine){
        CodeFragment[] codeFragment=new CodeFragment[2];
        codeFragment[0]=new CodeFragment();
        codeFragment[1]=new CodeFragment();
        String[] lineSplitted=pairLine.split(",");
        codeFragment[0].mainPath=mainPath;
        codeFragment[1].mainPath=mainPath;
        codeFragment[0].dirName=lineSplitted[0];
        codeFragment[0].fileName=lineSplitted[1];
        codeFragment[0].startline=Integer.parseInt(lineSplitted[2]);
        codeFragment[0].endline=Integer.parseInt(lineSplitted[3]);
        codeFragment[1].dirName=lineSplitted[4];
        codeFragment[1].fileName=lineSplitted[5];
        codeFragment[1].startline=Integer.parseInt(lineSplitted[6]);
        codeFragment[1].endline=Integer.parseInt(lineSplitted[7]);
        return codeFragment;

    }

    private void transform(final File file) throws FileNotFoundException {
        System.out.println("Transforming " + file.getName());
        JavaTransformationParser.FILE_COUNTER++;
        CompilationUnit cu = JavaParser.parse(file);
        TreeVisitor visitor = new CustomVisitor(file);
        visitor.visitPreOrder(cu);

    }

    private File writeTempFetchedFragment(CodeFragment codeFragment,String body){
        File file=null;
        PrintWriter pw =null;
        StringBuffer wrappedBody=new StringBuffer("");
        wrappedBody.append("public class Wrapper{"+System.lineSeparator());
        wrappedBody.append(body);
        wrappedBody.append("}");
        try {
            pw= new PrintWriter(codeFragment.dirName + "_" + codeFragment.fileName + "_" +
                    codeFragment.startline + "_" + codeFragment.endline + ".java");
            pw.write(wrappedBody.toString());
            file=new File(codeFragment.dirName + "_" + codeFragment.fileName + "_" +
                    codeFragment.startline + "_" + codeFragment.endline + ".java");
        }
        catch (IOException e){
            ExceptionLogger.log(e);
        }
        finally {
            pw.close();
        }
        return file;
    }

    private   static <K, V> Map<K, V> lruCache(final int maxSize) {
        return Collections.synchronizedMap(
                new LinkedHashMap<K, V>(maxSize * 4 / 3, 0.75f, true) {
                    @Override
                    protected boolean removeEldestEntry(
                            Map.Entry<K, V> eldest) {
                        return size() > maxSize;
                    }
                });
    }

}
