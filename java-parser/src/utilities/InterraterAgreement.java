package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Farima on 10/13/2018.
 */
public class InterraterAgreement {
    Connection dbconnection = null;
    InterraterAgreement()
    {
        getDBConnection();
    }

    static HashSet<String> type1Set=new HashSet<>();
    static HashSet<String> type2Set=new HashSet<>();
    static HashSet<String> type3Set=new HashSet<>();
    public static void main(String[] args) {
        InterraterAgreement interraterAgreement=new InterraterAgreement();
        String expid="";
        String path="";
        String toolName="";
        int numVoteforFP=0;
        ArrayList<String> type1Agreements=new ArrayList<>();
        ArrayList<String> type2Agreements=new ArrayList<>();
        ArrayList<String> type3Agreements=new ArrayList<>();
        if (args.length>0) {
            path = args[0];
//            path="D:\\ccaligner";
            toolName = args[1];
//            toolName="ccaligner";
            expid = args[2];
//            expid="106";
//            numVoteforFP=2;
//            numVoteforFP = Integer.parseInt(args[3]);
            interraterAgreement.fillTypeLists(path,toolName);
            ArrayList<String> fpsList=interraterAgreement.getAgreements(expid);
            for (String pair:fpsList){
                String[] pairSplit=pair.split(",");
                String reversePair=pairSplit[4]+","+pairSplit[5]+","+pairSplit[6]+","+pairSplit[7]+","+pairSplit[0]+","+pairSplit[1]+","+pairSplit[2]+","+pairSplit[3];
                if (type1Set.contains(pair) || type1Set.contains(reversePair)){
                    type1Agreements.add(pair);

                }
                else if (type2Set.contains(pair)|| type2Set.contains(reversePair)){
                    type2Agreements.add(pair);
                    //System.out.println(pair);
                }
                else if (type3Set.contains(pair)|| type3Set.contains(reversePair)){
                    type3Agreements.add(pair);
                }
                else {
                    System.out.println("pair not found, should not come here!!!!!!!!!!!!");
                    System.out.println(pair);
                }
            }

            System.out.println("Type 1 Agreement: "+((type1Agreements.size())/400f));
            System.out.println("Type 2 Agreement: "+((type2Agreements.size())/400f));
            System.out.println("Type 3 Agreement: "+((type3Agreements.size())/400f));
        }
    }

    private void fillTypeLists(String path,String toolName){

        try{
            BufferedReader bf=new BufferedReader(new FileReader(path+ File.separator+toolName+"-type1-samples.txt"));
            String line="";
            while ((line=bf.readLine())!=null){
                type1Set.add(line);
            }

            bf=new BufferedReader(new FileReader(path+ File.separator+toolName+"-type2-samples.txt"));
            line="";
            while ((line=bf.readLine())!=null){
                type2Set.add(line);
            }

            bf=new BufferedReader(new FileReader(path+File.separator+toolName+"-type3-samples.txt"));
            line="";
            while ((line=bf.readLine())!=null){
                type3Set.add(line);
            }

            System.out.println("type 1 size "+type1Set.size());
            System.out.println("type 2 size "+type2Set.size());
            System.out.println("type 3 size "+type3Set.size());
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }


    private void getDBConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            dbconnection = DriverManager.getConnection("jdbc:mysql://amazon.ics.uci.edu:3306/bcb?" +
                    "", "sourcerer", "donotpublish");
            System.out.println("Connected to MySql DB");

        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private ArrayList<String> getAgreements(String expid){
        ArrayList<String> result=new ArrayList<>();
        try {
            PreparedStatement pst = null;
            ResultSet rs = null;
            //when all say fp
            pst = dbconnection.prepareStatement("SELECT COUNT(*) As count,detail.vote, detail.candidate_pair_id,\n" +
                    "block1.folder_name AS b1_dir,block1.file_name AS b1_file,block1.start_line AS b1_start, block1.end_line AS b1_end,\n" +
                    "block2.folder_name AS b2_dir,block2.file_name AS b2_file,block2.start_line AS b2_start, block2.end_line AS b2_end\n" +
                    "FROM pf_prod.precisionCalculator_experimentdetail AS detail\n" +
                    "INNER JOIN pf_prod.precisionCalculator_candidatepair AS candpair\n" +
                    "ON detail.candidate_pair_id=candpair.id\n" +
                    "INNER JOIN pf_prod.precisionCalculator_block AS block1\n" +
                    "ON candpair.candidate_one_id=block1.id\n" +
                    "INNER JOIN pf_prod.precisionCalculator_block AS block2\n" +
                    "ON candpair.candidate_two_id=block2.id\n" +
                    "WHERE detail.experiment_id=? AND detail.vote=0 and detail.visited=1\n" +
                    "GROUP BY detail.candidate_pair_id, detail.vote,b1_dir,b1_file,b1_start,b1_end,b2_dir,b2_file,b2_start,b2_end\n" +
                    "HAVING count=3");
            pst.setString(1, expid);
            rs = pst.executeQuery();
            while (rs.next()) {
                result.add(rs.getString("b1_dir")+","+rs.getString("b1_file")+","+rs.getString("b1_start")+","+rs.getString("b1_end")+","+
                        rs.getString("b2_dir")+","+rs.getString("b2_file")+","+rs.getString("b2_start")+","+rs.getString("b2_end"));

            }
            //when all say tp
            pst = dbconnection.prepareStatement("SELECT COUNT(*) As count,detail.vote, detail.candidate_pair_id,\n" +
                    "block1.folder_name AS b1_dir,block1.file_name AS b1_file,block1.start_line AS b1_start, block1.end_line AS b1_end,\n" +
                    "block2.folder_name AS b2_dir,block2.file_name AS b2_file,block2.start_line AS b2_start, block2.end_line AS b2_end\n" +
                    "FROM pf_prod.precisionCalculator_experimentdetail AS detail\n" +
                    "INNER JOIN pf_prod.precisionCalculator_candidatepair AS candpair\n" +
                    "ON detail.candidate_pair_id=candpair.id\n" +
                    "INNER JOIN pf_prod.precisionCalculator_block AS block1\n" +
                    "ON candpair.candidate_one_id=block1.id\n" +
                    "INNER JOIN pf_prod.precisionCalculator_block AS block2\n" +
                    "ON candpair.candidate_two_id=block2.id\n" +
                    "WHERE detail.experiment_id=? AND detail.vote=1 and detail.visited=1\n" +
                    "GROUP BY detail.candidate_pair_id, detail.vote,b1_dir,b1_file,b1_start,b1_end,b2_dir,b2_file,b2_start,b2_end\n" +
                    "HAVING count=3");
            pst.setString(1, expid);
            rs = pst.executeQuery();
            while (rs.next()) {
                result.add(rs.getString("b1_dir")+","+rs.getString("b1_file")+","+rs.getString("b1_start")+","+rs.getString("b1_end")+","+
                        rs.getString("b2_dir")+","+rs.getString("b2_file")+","+rs.getString("b2_start")+","+rs.getString("b2_end"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
